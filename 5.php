<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 5</title>
    </head>
    <body>
        <?php
            echo "Quiero que coloque este texto en pantalla";
            //echo "<p align="center">"; # Esto esta mal
            echo '<p align="center">'; # esto es correcto
            //echo "<p align=\"center\">"; # esto es correcto
            echo "Academia Alpe";
            echo "</p>";
        ?>
    </body>
</html>
